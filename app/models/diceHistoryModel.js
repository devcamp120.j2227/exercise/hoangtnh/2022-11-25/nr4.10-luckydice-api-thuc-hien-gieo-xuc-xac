//khai báo thư viện mongoose
const mongoose = require("mongoose");

//khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khai báo dice history schema
const diceHistorySchema = new Schema ({
    user: {
        type: mongoose.Types.ObjectId, 
        ref: "User", 
        required: true
    },
	dice: {
        type:Number, 
        required: true
    },
    createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});

module.exports = mongoose.model("diceHistory", diceHistorySchema);