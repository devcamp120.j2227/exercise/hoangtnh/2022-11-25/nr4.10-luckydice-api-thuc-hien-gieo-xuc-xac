//import thư viện mongoose
const mongoose = require("mongoose");
//import class schema từ thư viện mongoose
const Schema = mongoose.Schema;

//tạo class prize Schema 
const prizeSchema = new Schema ({
    name: {
        type: String, 
        unique: true, 
        required: true
    },
	description: {
        type: String
    },
	createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("Prize", prizeSchema);