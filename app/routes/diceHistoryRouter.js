//khai báo thư viện express Js
const express = require("express");
//khai báo router chạy
const router = express.Router();
//khai báo dice history controller
const diceHisController = require("../controllers/diceHistoryController")

router.post("/dice-histories" , diceHisController.createDiceHistory);
router.get("/dice-histories", diceHisController.getAllDiceHis);
router.get("/dice-histories/:diceHisId", diceHisController.getDiceHisById);
router.put("/dice-histories/:diceHisId", diceHisController.updateDiceHisById)
router.delete("/dice-histories/:diceHisId", diceHisController.deleteDiceHisById);

module.exports = router;